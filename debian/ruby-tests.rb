require 'rbconfig'
ruby = File.join(RbConfig::CONFIG['bindir'], RbConfig::CONFIG['ruby_install_name'])

# instead of using Inline's regular directory in the user's home, use
# a temporary directory
require 'tmpdir'
ENV['INLINEDIR'] = Dir.mktmpdir

exec "#{ruby} -I. -Itest/ -rminitest/autorun ./test/test_heckle.rb"
